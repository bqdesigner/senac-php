<?php
    $jsonStr = '{"titulo":"Titanic","sinopse":"Navio colide com iceberg","ano":2019,"horarios":["16:00","19:00","20:00"]}';

    $arrayFilme = json_decode($jsonStr);
    // var_dump($arrayFilme);
 
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Json to Array </title>
    <style>
        ul {
            list-style: none;
        }
    </style>
</head>
<body>
    <h2> Filmes </h2>
    <ul>
        <li> <b> Título: </b> <?= $arrayFilme->titulo ?></li>
        <li> <b> Sinopse: </b> <?= $arrayFilme->sinopse ?></li>
        <li> <b> Ano: </b> <?= $arrayFilme->ano ?></li>
        <li> <b> Horário: </b> 
        <?php 
            for($i = 0; $i < count($arrayFilme->horarios); $i++) {
        ?> 
            <ul>
                <li> <?= $arrayFilme->horarios[$i] ?> </li>
            </ul>
        <?php
        }
        ?>
        </li>
    </ul>
</body>
</html>