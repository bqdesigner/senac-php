<?php
$arrayNome = ["Bruno", "Natália", "Talita"];

$arr = [];

// Primeiro loop preenche a array e o segundo loop percorre os valores com a array preenchida
// for ($i = 0; $i <= 100; $i++) {
//     $arr[] = $i;
// }
// for ($i = 0; $i <= 100; $i++) {
//     echo $arr[$i] . "<br />";
// }

// Exemplo função
// function exibirNome($nome) {
//     echo "Olá, o meu nome é {$nome}!";
// }
// echo exibirNome("Bruno");

// Pegando informações via POST
// Validando usando filtros
$nome = filter_input(INPUT_POST, "txtNome", FILTER_SANITIZE_STRING);
$email = filter_input(INPUT_POST, "txtEmail", FILTER_SANITIZE_STRING);
$funcionarioCod = filter_input(INPUT_POST, "slFuncionario", FILTER_SANITIZE_NUMBER_INT);
$funcionario = "";

if ($funcionarioCod) {
    $funcionario = $arrayNome[$funcionarioCod];
}

// Usando if pra validação
// if(isset($_POST["txtNome"])) {
//     $nome = $_POST["txtNome"];
// }

// if(isset($_POST["txtEmail"])) {
//     $email = $_POST["txtEmail"];
// }

// if(isset($_POST["slFuncionario"])) {
//     $funcionario = $arrayNome[$_POST["slFuncionario"]];
// }

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Scripts PHP</title>
    <style>
        ul {
            list-style: none;
        }
        input, select {
            padding: 5px;
            margin-top: 10px;
        }
    </style>
</head>
<body>
    <select name="slName" id="slName" style="display: block;">
    <?php
        for ($i = 0; $i < count($arrayNome); $i++) {
        ?>
        <option value="<?= $i; ?>"> <?= $arrayNome[$i]; ?> </option> 
        <?php
        }
    ?>
    </select>
    <button onclick="alert(document.getElementById('slName').value);"> Selecionar nome</button>

    <br> <br> 

    <form action="" method="post">
        <ul>
            <li> Nome: <input type="text" name="txtNome" id="txtNome"> </li>
            <li> E-mail: <input type="email" name="txtEmail" id="txtEmail"></li>
            <li> Funcionário:
                <select name="slFuncionario">
                    <?php
                        for ($i = 0; $i < count($arrayNome); $i++) {
                    ?>
                        <option value="<?= $i; ?>"> <?= $arrayNome[$i]; ?> </option> 
                    <?php
                    }
                    ?>
                </select>
            </li>
            <li> <input type="submit" value="Cadastrar" name="btnSubmit"></li>
        </ul>
    </form>
    <hr>
    <br>
    <p> Nome: <?= $nome ?></p>
    <p> E-mail: <?= $email ?></p>
    <p> Funcionário: <?= $funcionario ?></p>
    <hr>
    <br> <br>
    
</body>
</html>