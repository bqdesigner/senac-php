<?php
$cep = "06820-300";
// $cep = str_replace("-", "", $cep);
$str = file_get_contents('https://viacep.com.br/ws/'.$cep.'/json/');
echo($str);

$arrayCep = json_decode($str);

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Consulta CEP </title>
    <style>
        ul {
            list-style: none;
        }
    </style>
</head>

<body>

    <ul>
        <li> <b> Cep: </b> <?= $arrayCep->cep ?></li>
        <li> <b> Logradouro: </b> <?= $arrayCep->logradouro ?></li>
        <li> <b> Complemento: </b> <?= $arrayCep->complemento ?></li>
        <li> <b> Bairro: </b> <?= $arrayCep->bairro ?></li>
        <li> <b> Localidade: </b> <?= $arrayCep->localidade ?></li>
        <li> <b> UF: </b> <?= $arrayCep->uf ?></li>
    </ul>

</body>

</html>