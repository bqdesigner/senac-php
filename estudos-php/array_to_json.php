<?php
    $arrayFilme = array(
        "titulo" => "Titanic",
        "sinopse" => "Navio colide com iceberg",
        "ano" => 2019,
        "horarios" => array(
            "16:00",
            "19:00",
            "20:00"
        )
    );
    // var_dump($arrayFilme);

    $jsonStr = json_encode($arrayFilme);
    echo $jsonStr;
