<?php

class Usuario {

    var $id;
    var $nome;
    var $email;
    var $senha;

    // Construtor
    public function __construct(){
        echo "Aqui será feita a conexão com o banco de dados";
    }

    // Set
    public function setId (int $id) {
        $this->id = $id;
    }

    public function setNome (string $nome) {
        $this->nome = $nome;
    }

    public function setEmail (string $email) {
        $this->email = $email;
    }

    public function setSenha (string $senha) {
        $this->senha = $senha;
    }

    // Get

    public function getId (int $id): int {
        return $this->id;
    }

    public function getNome (string $nome): string {
        return $this->nome;
    }

    public function getEmail (string $email): string {
        return $this->email;
    }

    public function getSenha (string $senha): string {
        return $this->senha;
    }

    // Destructor
    public function __destruct(){
        echo "<br> Fechando conexão com o banco de dados";
    }

}