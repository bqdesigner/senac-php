<?php

// echo ("<pre>");
// var_dump ($_SERVER);
// echo "Seu IP é:  {$_SERVER['REMOTE_ADDR']}";

// phpinfo();

// Vetor com mais de uma dimensão

// $alunos[0]['nome'] = 'Bruno Queiros';
// $alunos[0]['bitbucket'] = 'https://bitbucket.org/bqdesigner/';
// $alunos[1]['nome'] = 'Rodrigo';
// $alunos[1]['bitbucket'] = 'https://bitbucket.org/rgavelino/';
// var_dump ($alunos);

// Outra forma com array
$alunos = array (0 => array('nome' => 'Bruno Queiros',
                            'bitbucket' => 'https://bitbucket.org/bqdesigner/'),
                 1 => array('nome' => 'Rodrigo',
                            'bitbucket' => 'https://bitbucket.org/rgavelino/'),
                 2 => array('nome' => 'João',
                            'bitbucket' => 'https://bitbucket.org/joao/'));
// Tabela com for
echo "<h2> Tabela com For </h2> <br>";
echo '<table border=1> 
      <thead>
        <th> Nome </th>
        <th> Bitbucket </th>
      </thead>';
for ($i = 0; $i < count($alunos); $i++) {
    echo "<tr> <td> {$alunos[$i]['nome']} </td>";
    echo "<td>{$alunos[$i]['bitbucket']} </td> </tr>";
}
echo '</table>';

// Tabela com forEach
echo "<h2> Tabela com Foreach </h2> <br>";
echo '<table border=1> 
      <thead>
        <th> Nome </th>
        <th> Bitbucket </th>
      </thead>';

foreach ($alunos as $ind => $linha) {
    // echo "Registro $ind: {$linha['nome']} <br>";
    echo "<tr> <td> {$linha['nome']} </td>";
    echo "<td>{$linha['bitbucket']} </td> </tr>";
}
echo '</table>';

// Tabela com while
echo "<h2> Tabela com while </h2> <br>";
echo '<table border=1> 
      <thead>
        <th> Nome </th>
        <th> Bitbucket </th>
      </thead>';
$i = 0;
while ($i < count($alunos)) {
    echo "<tr> <td> {$alunos[$i]['nome']} </td>";
    echo "<td>{$alunos[$i]['bitbucket']} </td> </tr>";
    $i++;
}
echo '</table>';