<?php

echo "<pre>";
define('HOST', '127.0.0.1');
define('USUARIO','root');
define('SENHA', '');
define('DB', 'aula_php');
define('PORTA', 3307);

if ($db = mysqli_connect(HOST, USUARIO, SENHA, DB, PORTA)) {
    
} else {
    echo "Não possível conectar";
    exit();
}

$login = null;
if ( isset($_POST['email'])) {

    $p = mysqli_prepare($db, 'SELECT id, nome, senha FROM usuarios WHERE email = ?');

    mysqli_stmt_bind_param($p, 's', $_POST['email']);

    mysqli_stmt_execute($p);
    $result= mysqli_stmt_get_result($p);
    $existe = $result->num_rows;

    $usuario = $result->fetch_assoc();

    // var_dump($usuario);

    if ($existe > 0 && $_POST['senha'] == $usuario['senha']) {

        $login = true;

    } else {

        $login = false;

    }
}

if($login === true) {
    session_start();
    $_SESSION['nome_usuario'] = $usuario['nome'];
    $_SESSION['id_usuario'] = $usuario['id'];
    require('menu.php');
    exit();
}

if ($login === false) {
    echo '<br> <br> Login ou senha inválidos';
}

echo '<form method="POST"> 
        E-mail: <input type="text" name="email"> <br>
        Senha: <input type="password" name="senha">
        <input type="submit" value="Entrar">
      </form>';

?>