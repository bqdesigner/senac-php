<?php
echo "<pre>";
// Outra forma com array
$alunos = array (0 => array('nome' => 'Bruno Queiros',
                            'bitbucket' => 'https://bitbucket.org/bqdesigner/'),
                 1 => array('nome' => 'Rodrigo',
                            'bitbucket' => 'https://bitbucket.org/rgavelino/'),
                 2 => array('nome' => 'João',
                            'bitbucket' => 'https://bitbucket.org/joao/'),
                 3 => array('nome' => 'Maria',
                            'bitbucket' => 'https://bitbucket.org/maria/'));


// Modificando a tabela com If ternário
$cor = "green";
// If ternário
$cor = $cor == "green" ? "red" : "yellow";

echo "<h2> Modificando a tabela com If ternário </h2> <br>";
echo "<table border=1 bordercolor=$cor> 
      <thead>
        <th> Nome </th>
        <th> Bitbucket </th>
      </thead>";

$cor2 = null;
foreach ($alunos as $ind => $linha) {
    // Deixando cada linha com uma cor diferente com if ternário
    $cor2 = $cor2 == "yellow" ? "gray" : "yellow";
    echo "<tr bgcolor=$cor2> <td> {$linha['nome']} </td>";
    echo "<td>{$linha['bitbucket']} </td> </tr>";
}
echo '</table>';

// Modificando a tabela com If/Else
echo "<h2> Modificando a tabela com If/Else </h2> <br>";
echo "<table border=1> 
      <thead>
        <th> Nome </th>
        <th> Bitbucket </th>
      </thead>";

$cor3 = null;
foreach ($alunos as $ind => $linha) {
    // Deixando cada linha com uma cor diferente com if/else
    if ($cor3 == "white") 
        $cor3 = "LightSkyBlue";
    else 
        $cor3 = "white";
    
    echo "<tr bgcolor=$cor3> <td> {$linha['nome']} </td>";
    echo "<td>{$linha['bitbucket']} </td> </tr>";
}
echo '</table>';

echo "<br> <br>";

// String position
$nome = "Bruno de Queirós";

$posicao = strpos($nome, 'uno');
if ($posicao) {
    echo "Encontrado na posição <strong> $posicao</strong>";
}
