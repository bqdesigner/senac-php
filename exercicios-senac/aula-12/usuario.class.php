<?php

class Usuario {

    private $id;
    private $nome;
    private $email;
    private $senha;
    private $objDb;

    // Construtor
    public function __construct(){

        $this->objDb = new mysqli('localhost', 'root', '', 'aula_php', 3307);

    }

    // Set
    public function setId (int $id) {
        $this->id = $id;
    }

    public function setNome (string $nome) {
        $this->nome = $nome;
    }

    public function setEmail (string $email) {
        $this->email = $email;
    }

    public function setSenha (string $senha) {
        $this->senha = password_hash($senha, PASSWORD_DEFAULT);
    }

    // Get

    public function getId (int $id): int {
        return $this->id;
    }

    public function getNome (string $nome): string {
        return $this->nome;
    }

    public function getEmail (string $email): string {
        return $this->email;
    }

    public function getSenha (string $senha): string {
        return $this->senha;
    }

    public function getUser (int $id): int {
        return array ($this->id, $this->nome, $this->email);
    }

    // Função para salvar no Banco de dados
    public function saveUser() {
        // Prepara a consulta
        $objStmt = $this->objDb->prepare('REPLACE INTO usuarios(id, nome, email, senha) VALUES (?, ?, ?, ?)');
        // Criando o bind da consulta
        $objStmt->bind_param('isss', $this->id, $this->nome, $this->email, $this->senha);
        // Se executou, retorna true
        if($objStmt->execute()) {
            return true;
        } else {
            return false;
        }

    }
    // Função para excluir
    public function delUser() {
        $objStmt = $this->objDb->prepare('DELETE FROM usuarios where id=?');
        $objStmt->bind_param('i', $this->id);
        if($objStmt->execute()) {
            return true;
        } else {
            return false;
        }
    }
    // Função para listar os usuários
    public function listUser() {
        $objStmt = $this->objDb->query("SELECT id, nome, email, senha FROM usuarios WHERE id = {$this->id}");
        return $objStmt;
    }



    // Destructor
    public function __destruct(){
        unset($this->objDb);
    }

}