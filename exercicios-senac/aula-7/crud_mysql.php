<?php
// CRUD com Mysql
echo "<pre>";

define('HOST', '127.0.0.1');
define('USUARIO','root');
define('SENHA', '');
define('DB', 'aula_php');
define('PORTA', 3307);
// $db = mysqli_connect(HOST, USUARIO, SENHA, DB) or die ("Não foi possível conectar");

if ($db = mysqli_connect(HOST, USUARIO, SENHA, DB, PORTA)) {
    
} else {
    echo "Não possível conectar";
    exit();
}

$id = $dados['nome'] = $dados['url'] = null;
    // Pega o ID do cadastro que deseja editar
    if (isset($_POST['editar'])) {
        $id = preg_replace('/\D/', '', $_POST['editar']);
        $prep = mysqli_prepare($db, 'SELECT nome, url FROM tb_bitbucket WHERE id = ?');
        mysqli_stmt_bind_param($prep, 'i', $id);
        mysqli_stmt_execute($prep);
        $result = mysqli_stmt_get_result($prep);
        $dados = $result->fetch_assoc();
    }
    echo "<form method='post'>
            <input type='hidden' name='id' value='{$id}' /> 
            Nome: <input type='text' name='nome' value='{$dados['nome']}'> <br>
            Bitbucket: <input type='text' name='url' value='{$dados['url']}'> <br>
            <input type='submit' value='Cadastrar'>
        </form>";

    $nome = isset($_POST['nome']) ? $_POST['nome'] : null;
    $url = isset($_POST['url']) ? $_POST['url'] : null;

    if (empty($_POST['id'])) {

        // Preparando a consulta no banco de dados
        $preparada = mysqli_prepare($db, ' INSERT INTO tb_bitbucket (nome, url, ip) VALUES (?, ?, ?)');
        // Unindo a consulta aos valores informados
        mysqli_stmt_bind_param($preparada, 'sss', $nome, $url, $_SERVER['REMOTE_ADDR']);

        // Executando
        if (mysqli_stmt_execute($preparada)) {
            echo ("<br> Dados de $nome salvos no Banco de dados");
            header('Location: crud_mysql.php');
            exit();
        }
    } elseif ( is_numeric($_POST['id']) ) {

        $att = mysqli_prepare($db, " UPDATE tb_bitbucket SET nome=?, url=? WHERE id=?");
        mysqli_stmt_bind_param($att, 'i', $id);
        mysqli_stmt_execute($att);
        $resultAtt = mysqli_stmt_get_result($att);
        if (mysqli_stmt_execute($att)) {
            echo ("<br> Dados de $nome salvos no Banco de dados");
            header('Location: crud_mysql.php');
            exit();
        }
    }

   
    // Fazer consulta preparada para evitar SQL Injection
    $objConsulta = mysqli_query($db, 'SELECT id, nome, url, ip FROM tb_bitbucket');

    // $reg1 = $objConsulta->fetch_assoc();
    // var_dump($reg1);

        // var_dump($_POST['editar']);

    echo "<form method='POST'>";

    // Criando tabela
    echo "<table border=1> 
        <thead>
            <th> ID </th>
            <th> Nome </th>
            <th> URL </th>
            <th> IP </th>
            <th> Editar </th>
            <th> Excluir </th>
        </thead>";
    while ($reg = $objConsulta->fetch_assoc()) {
        echo "<tr> <td> {$reg['id']} </td>
            <td>{$reg['nome']} </td>
            <td>{$reg['url']} </td>
            <td>{$reg['ip']} </td>
            <td> <button name='editar' value='{$reg['id']}'> Editar </button></td>
            <td> <button name='excluir' value='{$reg['id']}'> Excluir </button></td></tr>";
    }
    echo "</table>";
    echo "</form>";
    echo "</pre>";
    // echo "Deu certo";
  
