<?php
// CRUD com Mysql
echo "<pre>";

define('HOST', '127.0.0.1');
define('USUARIO','root');
define('SENHA', '');
define('DB', 'aula_php');
define('PORTA', 3307);
// $db = mysqli_connect(HOST, USUARIO, SENHA, DB) or die ("Não foi possível conectar");


if ($db = mysqli_connect(HOST, USUARIO, SENHA, DB, PORTA)) {
    echo "Deu certo";
} else {
    echo "Não possível conectar";
}
// exit();

echo ' <form method="post">
        Nome: <input type="text" name="nome"> <br>
        Bitbucket: <input type="text" name="url"> <br>
        <input type="submit" value="Enviar">
       </form>';

$nome = isset($_POST['nome']) ? $_POST['nome'] : null;
$url = isset($_POST['url']) ? $_POST['url'] : null;

// Preparando a consulta no banco de dados
$preparada = mysqli_prepare($db, ' INSERT INTO tb_bitbucket (nome, url, ip) VALUES (?, ?, ?)');

// Unindo a consulta aos valores informados
mysqli_stmt_bind_param($preparada, 'sss', $nome, $url, $_SERVER['REMOTE_ADDR']);

// Executando
if (mysqli_stmt_execute($preparada)) {
    echo ("<br> Dados de $nome salvos no Banco de dados");
    exit();
}

// Fazer consulta preparada para evitar SQL Injection
$objConsulta = mysqli_query($db, 'SELECT id, nome, url, ip FROM tb_bitbucket');

// $reg1 = $objConsulta->fetch_assoc();
// var_dump($reg1);

echo "<table border=1> 
      <thead>
        <th> ID </th>
        <th> Nome </th>
        <th> URL </th>
        <th> IP </th>
      </thead>";
while ($reg1 = $objConsulta->fetch_assoc()) {
    echo "<tr> <td> {$reg1['id']} </td>";
    echo "<td>{$reg1['nome']} </td>";
    echo "<td>{$reg1['url']} </td> ";
    echo "<td>{$reg1['ip']} </td></tr>";
}
echo "</table>";
echo "</pre>";