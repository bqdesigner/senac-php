<?php
echo "<pre>";
// Constante PHP
define('CONSTANT_PAGE', 10);

echo "Valor da minha constante é " . CONSTANT_PAGE;

// Variável para passar valor para uma constante
$qtd_paginas = 25;

echo "\n A quantidade de páginas é " . $qtd_paginas ;

// Passando uma constante para define
$ip_do_banco = '192.168.45.12';
define('IP_DO_BANCO', $ip_do_banco);
echo "\n O IP do SGBG é " . IP_DO_BANCO;

// Constantes mágicas
echo "\n Estou na linha " . __LINE__;
echo "\n Esse é o arquivo " . __FILE__ . "\n";

var_dump($ip_do_banco) . "\n";

$dias_da_semana = ['seg', 'ter', 'qua', 'quin', 'sex', 'sab', 'dom'];
var_dump($dias_da_semana);
